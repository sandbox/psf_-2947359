Accountant module

Financial account management


Documentation references:
T-Account => https://www.investopedia.com/terms/t/t-account.asp
What is a 'T-Account'
A T-account is an informal term for a set of financial records that use
double-entry bookkeeping. The term T-account describes the appearance of the
bookkeeping entries. If a large letter T were drawn on the page, the account
title would appear just above the T, debits would be listed under the top line
of the T on the left side and the credits would be listed under the top line of
the T on the right side, with the middle line separating the debits from the
credits.

List builder using Views => https://www.drupal.org/project/entity/issues/2959628
