<?php

/**
 * @file
 * Contains accountant.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;

// Load business.page.inc.
module_load_include('inc', 'accountant', 'business.page');
// Load account.page.inc.
module_load_include('inc', 'accountant', 'account.page');
// Load move.page.inc.
module_load_include('inc', 'accountant', 'move.page');

/**
 * Implements hook_help().
 */
function accountant_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the accountant module.
    case 'help.page.accountant':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Financial account management') . '</p>';
      return $output;

    default:
  }

  return '';
}

/**
 * Implements hook_theme().
 */
function accountant_theme() {
  return [
    'business' => [
      'render element' => 'elements',
    ],
    'account' => [
      'render element' => 'elements',
    ],
    'move' => [
      'render element' => 'elements',
    ],
  ];
}

/**
 * Implements hook_entity_extra_field_info().
 *
 * Add a pseudo-field to business to display the accounts list.
 * Add a pseudo-field to account to display the moves list.
 *
 * @see https://medium.com/manati-web-agency/https-medium-com-manati-web-agency-drupal-8-pseudo-fields-templates-and-rendering-images-with-links-8d02d083d2f
 */
function accountant_entity_extra_field_info() {
  $extra = [];

  $extra['business']['business']['display']['fake_accounts'] = [
    'label' => t('Accounts'),
    'description' => t('Business accounts.'),
    'weight' => 100,
    'visible' => TRUE,
  ];

  $extra['account']['account']['display']['fake_moves'] = [
    'label' => t('Moves'),
    'description' => t('Accounts moves.'),
    'weight' => 100,
    'visible' => TRUE,
  ];

  return $extra;
}

/**
 * Implements hook_ENTITY_TYPE_view().
 *
 * Show the business pseudo-field to display the accounts list.
 *
 * @see https://www.webomelette.com/creating-pseudo-fields-drupal-8
 */
function accountant_business_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  if ($display->getComponent('fake_accounts')) {
    $build['fake_accounts'] = [
      '#type' => 'markup',
      '#markup' => '// @todo: Add the business account list here. [accountant_business_view()]',
    ];
  }
}

/**
 * Implements hook_ENTITY_TYPE_view().
 *
 * Show the account pseudo-field to display the moves list.
 *
 * @see https://www.webomelette.com/creating-pseudo-fields-drupal-8
 */
function accountant_account_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  if ($display->getComponent('fake_moves')) {
    $build['fake_moves'] = [
      '#type' => 'markup',
      '#markup' => '// @todo: Add the account moves list here. [accountant_account_view()]',
    ];
  }
}

/**
 * Prevents validation of decimal numbers.
 *
 * @see https://www.drupal.org/node/2230909
 */
function accountant_field_widget_form_alter(&$element, FormStateInterface $form_state, $context) {
  /** @var \Drupal\field\Entity\FieldConfig $field_definition */
  $field_definition = $context['items']->getFieldDefinition();

  if (method_exists($field_definition, 'getProvider')) {
    // Only change accountant entities fields.
    if ($field_definition->getType() == 'decimal' && $field_definition->getProvider() == 'accountant') {
      $element['value']['#step'] = 'any';
    }
  }
}
