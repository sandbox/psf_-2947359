<?php

/**
 * @file
 * Contains business.page.inc.
 *
 * Page callback for Business entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Business templates.
 *
 * Default template: business.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_business(array &$variables) {
  // Fetch BusinessEntity Entity Object.
  // $business = $variables['elements']['#business'];.
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
