<?php

namespace Drupal\accountant\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Business entity.
 *
 * @ingroup accountant
 *
 * @ContentEntityType(
 *   id = "business",
 *   label = @Translation("Business"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\accountant\BusinessEntityListBuilder",
 *     "views_data" = "Drupal\accountant\Entity\BusinessEntityViewsData",
 *     "translation" = "Drupal\accountant\BusinessEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\accountant\Form\BusinessEntityForm",
 *       "add" = "Drupal\accountant\Form\BusinessEntityForm",
 *       "edit" = "Drupal\accountant\Form\BusinessEntityForm",
 *       "delete" = "Drupal\accountant\Form\BusinessEntityDeleteForm",
 *     },
 *     "access" = "Drupal\accountant\BusinessEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\accountant\BusinessEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "accountant_business",
 *   data_table = "business_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer business entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/accountant/business/{business}",
 *     "add-form" = "/admin/structure/accountant/business/add",
 *     "edit-form" = "/admin/structure/accountant/business/{business}/edit",
 *     "delete-form" = "/admin/structure/accountant/business/{business}/delete",
 *     "collection" = "/admin/structure/accountant/business",
 *   },
 *   field_ui_base_route = "business.settings"
 * )
 */
class BusinessEntity extends ContentEntityBase implements BusinessEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccounts() {
    $query = \Drupal::entityQuery('account');
    $query = $query->condition('business_id', $this->id(), '=');
    $results = $query->execute();
    $accounts = [];

    foreach ($results as $rid => $id) {
      $accounts[] = AccountEntity::load($rid);
    }

    return $accounts;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllMovesIds() {
    $query = \Drupal::entityQuery('move');
    $gout = $query->orConditionGroup()
      ->condition('source_business_id', $this->id(), '=')
      ->condition('destination_business_id', $this->id(), '=');
    $query = $query->condition($gout);
    $results = $query->execute();

    return array_values($results);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owned by'))
      ->setDescription(t('The user ID of owner of the Business entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Business entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
