<?php

namespace Drupal\accountant\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\accountant\EntityBusinessInterface;

/**
 * Provides an interface for defining Account entities.
 *
 * @ingroup accountant
 */
interface AccountEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityBusinessInterface {

  /**
   * Gets the Account name.
   *
   * @return string
   *   Name of the Account.
   */
  public function getName();

  /**
   * Sets the Account name.
   *
   * @param string $name
   *   The Account name.
   *
   * @return \Drupal\accountant\Entity\AccountEntityInterface
   *   The called Account entity.
   */
  public function setName($name);

  /**
   * Gets the Account creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Account.
   */
  public function getCreatedTime();

  /**
   * Sets the Account creation timestamp.
   *
   * @param int $timestamp
   *   The Account creation timestamp.
   *
   * @return \Drupal\accountant\Entity\AccountEntityInterface
   *   The called Account entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Get the Account balance type.
   *
   * @return string
   *   The balance type ID.
   */
  public function getBalanceTypeId();

  /**
   * Get the Account balance type.
   *
   * @return string
   *   The balance type label.
   */
  public function getBalanceTypeLabel();

  /**
   * Set the Account balance type.
   *
   * @param string $id
   *   The balance type ID.
   *
   * @return string
   *   The balance type ID.
   */
  public function setBalanceTypeId($id);

}
