<?php

namespace Drupal\accountant\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Account entity.
 *
 * @ingroup accountant
 *
 * @ContentEntityType(
 *   id = "account",
 *   label = @Translation("Account"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\accountant\AccountEntityListBuilder",
 *     "views_data" = "Drupal\accountant\Entity\AccountEntityViewsData",
 *     "translation" = "Drupal\accountant\AccountEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\accountant\Form\AccountEntityForm",
 *       "add" = "Drupal\accountant\Form\AccountEntityForm",
 *       "edit" = "Drupal\accountant\Form\AccountEntityForm",
 *       "delete" = "Drupal\accountant\Form\AccountEntityDeleteForm",
 *     },
 *     "access" = "Drupal\accountant\AccountEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\accountant\AccountEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "accountant_account",
 *   data_table = "account_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer account entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "business" = "business_id",
 *     "langcode" = "langcode",
 *     "balance" = "balance_type",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/accountant/account/{account}",
 *     "add-form" = "/admin/structure/accountant/account/add",
 *     "edit-form" = "/admin/structure/accountant/account/{account}/edit",
 *     "delete-form" = "/admin/structure/accountant/account/{account}/delete",
 *     "collection" = "/admin/structure/accountant/account",
 *   },
 *   field_ui_base_route = "account.settings"
 * )
 */
class AccountEntity extends ContentEntityBase implements AccountEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBusiness() {
    return $this->get('business_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getBusinessId() {
    return $this->get('business_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setBusinessId($uid) {
    $this->set('business_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setBusiness(BusinessEntityInterface $business) {
    $this->set('business_id', $business->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBalanceTypeId() {
    return $this->get('balance_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getBalanceTypeLabel() {
    $values = self::getBalanceTypeEnumValues();
    return $values[$this->getBalanceTypeId()];
  }

  /**
   * {@inheritdoc}
   */
  public function setBalanceTypeId($id) {
    $this->set('balance_type', $id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['balance_type'] = BaseFieldDefinition::create("list_string")
      ->setSettings([
        'allowed_values' => self::getBalanceTypeEnumValues(),
      ])
      ->setDefaultValue([['value' => 'neutral']])
      ->setLabel(t('Balance type'))
      ->setDescription(t('Select the balance influence of this account'))
      ->setRequired(TRUE)
      ->setCardinality(-1)
      ->setDisplayOptions('view', [
        'label' => 'show',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['business_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Business'))
      ->setDescription(t('The business ID of business of the Account entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'business')
      ->setSetting('handler', 'default')
      ->setTranslatable(FALSE)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_integer',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Account entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * Get the balance type options.
   *
   * @return array
   *   The balance types and keys.
   */
  public static function getBalanceTypeEnumValues() {
    return [
      'neutral' => t('Neutral'),
      'debits' => t('Debits'),
      'credits' => t('Credits'),
    ];
  }

}
