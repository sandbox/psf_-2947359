<?php

namespace Drupal\accountant\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface for defining Move entities.
 *
 * @ingroup accountant
 */
interface MoveEntityInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the Move name.
   *
   * @return string
   *   Name of the Move.
   */
  public function getName();

  /**
   * Sets the Move name.
   *
   * @param string $name
   *   The Move name.
   *
   * @return \Drupal\accountant\Entity\MoveEntityInterface
   *   The called Move entity.
   */
  public function setName($name);

  /**
   * Gets the Move creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Move.
   */
  public function getCreatedTime();

  /**
   * Sets the Move creation timestamp.
   *
   * @param int $timestamp
   *   The Move creation timestamp.
   *
   * @return \Drupal\accountant\Entity\MoveEntityInterface
   *   The called Move entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Get the source account.
   *
   * @return \Drupal\accountant\Entity\AccountEntityInterface
   *   The account entity.
   */
  public function getSourceAccount();

  /**
   * Set the source account.
   *
   * @param \Drupal\accountant\Entity\AccountEntityInterface $account
   *   The account entity.
   *
   * @return \Drupal\accountant\Entity\MoveEntityInterface
   *   This move.
   */
  public function setSourceAccount(AccountEntityInterface $account);

  /**
   * Get the source account ID.
   *
   * @return int|null
   *   The source account ID.
   */
  public function getSourceAccountId();

  /**
   * Set the source account by ID.
   *
   * @param int $uid
   *   The source account ID.
   *
   * @return \Drupal\accountant\Entity\MoveEntityInterface
   *   This move.
   */
  public function setSourceAccountId($uid);

  /**
   * Get the Destination account.
   *
   * @return \Drupal\accountant\Entity\AccountEntityInterface
   *   The account entity.
   */
  public function getDestinationAccount();

  /**
   * Set the Destination account.
   *
   * @param \Drupal\accountant\Entity\AccountEntityInterface $account
   *   The account entity.
   *
   * @return \Drupal\accountant\Entity\MoveEntityInterface
   *   This move.
   */
  public function setDestinationAccount(AccountEntityInterface $account);

  /**
   * Get the Destination account ID.
   *
   * @return int|null
   *   The Destination account ID.
   */
  public function getDestinationAccountId();

  /**
   * Set the Destination account by ID.
   *
   * @param int $uid
   *   The Destination account ID.
   *
   * @return \Drupal\accountant\Entity\MoveEntityInterface
   *   This move.
   */
  public function setDestinationAccountId($uid);

  /**
   * Gets the Move amount.
   *
   * @return string
   *   Amount of the Move.
   */
  public function getAmount();

  /**
   * Sets the Move amount.
   *
   * @param float $amount
   *   The Move amount.
   *
   * @return \Drupal\accountant\Entity\MoveEntityInterface
   *   The called Move entity.
   */
  public function setAmount($amount);

  /**
   * Returns the entity business entity.
   *
   * @return \Drupal\user\UserInterface
   *   The owner user entity.
   */
  public function getSourceBusiness();

  /**
   * Returns the entity business ID.
   *
   * @return int|null
   *   The business ID, or NULL in case the business ID field has not been set
   *   on the entity.
   */
  public function getSourceBusinessId();

  /**
   * Returns the entity business entity.
   *
   * @return \Drupal\user\UserInterface
   *   The owner user entity.
   */
  public function getDestinationBusiness();

  /**
   * Returns the entity business ID.
   *
   * @return int|null
   *   The business ID, or NULL in case the business ID field has not been set
   *   on the entity.
   */
  public function getDestinationBusinessId();

  /**
   * Returns the source balance.
   *
   * @return float
   *   The source account balance.
   */
  public function getSourceBalance();

  /**
   * Returns the destination balance.
   *
   * @return float
   *   The destination account balance.
   */
  public function getDestinationBalance();

}
