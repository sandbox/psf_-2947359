<?php

namespace Drupal\accountant\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the Move entity.
 *
 * @ingroup accountant
 *
 * @ContentEntityType(
 *   id = "move",
 *   label = @Translation("Move"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\accountant\MoveEntityListBuilder",
 *     "views_data" = "Drupal\accountant\Entity\MoveEntityViewsData",
 *
 *     "storage_schema" = "Drupal\accountant\MoveStorageSchema",
 *
 *     "form" = {
 *       "default" = "Drupal\accountant\Form\MoveEntityForm",
 *       "add" = "Drupal\accountant\Form\MoveEntityForm",
 *       "edit" = "Drupal\accountant\Form\MoveEntityForm",
 *       "delete" = "Drupal\accountant\Form\MoveEntityDeleteForm",
 *     },
 *     "access" = "Drupal\accountant\MoveEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\accountant\MoveEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "accountant_move",
 *   admin_permission = "administer move entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "source" = "source_id",
 *     "destination" = "destination_id",
 *     "source_business" = "source_business_id",
 *     "destination_business" = "destination_business_id",
 *     "amount" = "amount",
 *     "source_balance" = "source_balance",
 *     "destination_balance" = "destination_balance",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/accountant/move/{move}",
 *     "add-form" = "/admin/structure/accountant/move/add",
 *     "edit-form" = "/admin/structure/accountant/move/{move}/edit",
 *     "delete-form" = "/admin/structure/accountant/move/{move}/delete",
 *     "collection" = "/admin/structure/accountant/move",
 *   },
 *   field_ui_base_route = "move.settings"
 * )
 */
class MoveEntity extends ContentEntityBase implements MoveEntityInterface {
  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceAccount() {
    return $this->get('source_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceAccountId() {
    return $this->get('source_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setSourceAccountId($uid) {
    $this->set('source_id', $uid);
    // We need update the source business too.
    $this->updateSourceBusiness($this->getSourceAccount());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setSourceAccount(AccountEntityInterface $account) {
    $this->set('source_id', $account->id());
    // We need update the source business too.
    $this->updateSourceBusiness($this->getSourceAccount());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationAccount() {
    return $this->get('destination_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationAccountId() {
    return $this->get('destination_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setDestinationAccountId($uid) {
    $this->set('destination_id', $uid);
    // We need update the destination business too.
    $this->updateDestinationBusiness($this->getDestinationAccount());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setDestinationAccount(AccountEntityInterface $account) {
    $this->set('destination_id', $account->id());
    // We need update the destination business too.
    $this->updateDestinationBusiness($this->getDestinationAccount());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAmount() {
    return $this->get('amount')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAmount($amount) {
    $this->set('amount', $amount);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceBusiness() {
    return $this->get('source_business_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceBusinessId() {
    return $this->get('source_business_id')->target_id;
  }

  /**
   * Set de business of the account how our own business.
   *
   * @param \Drupal\accountant\Entity\AccountEntityInterface $account
   *   The reference account.
   *
   * @return $this
   *   The called Move entity.
   */
  protected function updateSourceBusiness(AccountEntityInterface $account) {
    if ($account) {
      $this->setSourceBusiness($account->getBusiness());
    }
    return $this;
  }

  /**
   * Set de business of the account how our own business.
   *
   * @param \Drupal\accountant\Entity\AccountEntityInterface $account
   *   The reference account.
   *
   * @return $this
   *   The called Move entity.
   */
  protected function updateDestinationBusiness(AccountEntityInterface $account) {
    if ($account) {
      $this->setDestinationBusiness($account->getBusiness());
    }
    return $this;
  }

  /**
   * Sets the entity business ID.
   *
   * @param int $uid
   *   The business id.
   *
   * @return $this
   */
  protected function setSourceBusinessId($uid) {
    $this->set('source_business_id', $uid);
    return $this;
  }

  /**
   * Sets the entity business entity.
   *
   * @param \Drupal\accountant\entity\BusinessEntityInterface $business
   *   The business entity.
   *
   * @return $this
   */
  protected function setSourceBusiness(BusinessEntityInterface $business) {
    $this->set('source_business_id', $business->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationBusiness() {
    return $this->get('destination_business_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationBusinessId() {
    return $this->get('destination_business_id')->target_id;
  }

  /**
   * Sets the entity business ID.
   *
   * @param int $uid
   *   The business id.
   *
   * @return $this
   */
  protected function setDestinationBusinessId($uid) {
    $this->set('destination_business_id', $uid);
    return $this;
  }

  /**
   * Sets the entity business entity.
   *
   * @param \Drupal\accountant\entity\BusinessEntityInterface $business
   *   The business entity.
   *
   * @return $this
   */
  protected function setDestinationBusiness(BusinessEntityInterface $business) {
    $this->set('destination_business_id', $business->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceBalance() {
    // @todo: Calculate move balance.
    return 10;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationBalance() {
    // @todo: Calculate move balance.
    return 11;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // We need update the business too.
    $this->updateSourceBusiness($this->getSourceAccount());
    $this->updateDestinationBusiness($this->getDestinationAccount());
  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities) {
    foreach ($entities as $entity) {
      $entity->values['source_balance']['x-default'] = $entity->getSourceBalance();
      $entity->values['destination_balance']['x-default'] = $entity->getDestinationBalance();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['source_business_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Source business'))
      ->setDescription(t('The business ID of business of the source Account entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'business')
      ->setSetting('handler', 'default')
      ->setTranslatable(FALSE)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'show',
        'type' => 'number_integer',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['destination_business_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Destination business'))
      ->setDescription(t('The business ID of business of the destination Account entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'business')
      ->setSetting('handler', 'default')
      ->setTranslatable(FALSE)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'show',
        'type' => 'number_integer',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['amount'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Amount'))
      ->setDescription(t('The move amount.'))
      ->setSettings([
        // Allow 9999999999,999999 money units.
        'precision' => 16,
        'scale' => 6,
        'step' => 'any',
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_decimal',
        'weight' => -2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Concept'))
      ->setDescription(t('The move concept.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'show',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['source_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Source account'))
      ->setDescription(t('The account ID of source.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'account')
      // @todo: Add a handler to allow only one business accounts. ¿?
      ->setSetting('handler', 'default')
      ->setTranslatable(FALSE)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'show',
        'type' => 'number_integer',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['destination_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Destination account'))
      ->setDescription(t('The account ID of destination.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'account')
      // @todo: Add a handler to allow only one business accounts. ¿?
      ->setSetting('handler', 'default')
      ->setTranslatable(FALSE)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'show',
        'type' => 'number_integer',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['source_balance'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Source balance'))
      ->setDescription(t('The move source account balance.'))
      ->setSettings([
        // Allow 9999999999,999999 money units.
        'precision' => 16,
        'scale' => 6,
        'step' => 'any',
      ])
      ->setDefaultValue(0)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_decimal',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['destination_balance'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Destination balance'))
      ->setDescription(t('The move destination account balance.'))
      ->setSettings([
        // Allow 9999999999,999999 money units.
        'precision' => 16,
        'scale' => 6,
        'step' => 'any',
      ])
      ->setDefaultValue(0)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_decimal',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
