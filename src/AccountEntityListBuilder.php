<?php

namespace Drupal\accountant;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Account entities.
 *
 * @ingroup accountant
 */
class AccountEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['business_id'] = $this->t('Business');
    $header['name'] = $this->t('Name');
    $header['balance_type'] = $this->t('Balance');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $business \Drupal\accountant\Entity\BusinessEntity */
    $business = $entity->getBusiness();
    /* @var $entity \Drupal\accountant\Entity\AccountEntity */
    if ($business) {
      $row['business_id'] = Link::createFromRoute(
        $business->label(),
        'entity.business.canonical',
        ['business' => $business->id()]
      );
    }
    else {
      $row['business_id'] = $this->t('Error');
    }
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.account.canonical',
      ['account' => $entity->id()]
    );
    $row['balance_type'] = $entity->getBalanceTypeLabel();
    return $row + parent::buildRow($entity);
  }

}
