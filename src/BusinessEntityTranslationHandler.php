<?php

namespace Drupal\accountant;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for business.
 */
class BusinessEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
