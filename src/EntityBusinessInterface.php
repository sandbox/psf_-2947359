<?php

namespace Drupal\accountant;

use Drupal\accountant\Entity\BusinessEntityInterface;

/**
 * Defines a common interface for entities that have an business.
 */
interface EntityBusinessInterface {

  /**
   * Returns the entity business entity.
   *
   * @return \Drupal\user\UserInterface
   *   The owner user entity.
   */
  public function getBusiness();

  /**
   * Sets the entity business entity.
   *
   * @param \Drupal\accountant\entity\BusinessEntityInterface $business
   *   The business entity.
   *
   * @return $this
   */
  public function setBusiness(BusinessEntityInterface $business);

  /**
   * Returns the entity business ID.
   *
   * @return int|null
   *   The business ID, or NULL in case the business ID field has not been set
   *   on the entity.
   */
  public function getBusinessId();

  /**
   * Sets the entity business ID.
   *
   * @param int $uid
   *   The business id.
   *
   * @return $this
   */
  public function setBusinessId($uid);

}
