<?php

/**
 * @file
 * Generate moves batch.
 */

use Drupal\accountant\Entity\MoveEntity;

/**
 * Dedlete moves.
 *
 * @param array $options
 *   New move instructions.
 * @param array $context
 *   Context.
 */
function delete_moves_batch(array $options, array &$context) {

  foreach ($options as $moveId) {
    $move = MoveEntity::load($moveId);
    $move->delete();
  }

  $context['message'] = t('Deleting @cnt moves.', [
    '@cnt' => count($options),
  ]);
}

/**
 * Callback when the batch processing is complete.
 *
 * @param mixed $success
 *   Success.
 * @param mixed $results
 *   Results.
 * @param mixed $operations
 *   Operations.
 */
function delete_moves_batch_completed_callback($success, $results, $operations) {
  if ($success) {
    // Do something else if needed.
    $message = t('All moves erased.');
  }
  else {
    $message = t('There were some errors.');
  }
  drupal_set_message($message);
}
