<?php

/**
 * @file
 * Generate moves batch.
 */

use Drupal\accountant\Entity\AccountEntity;
use Drupal\accountant\Entity\MoveEntity;

/**
 * Generate a demo move.
 *
 * @param array $options
 *   New move instructions.
 * @param array $context
 *   Context.
 */
function generate_moves_batch(array $options, array &$context) {

  // Generate one move each time.
  $source = AccountEntity::load($options['source']);
  $destination = AccountEntity::load($options['destination']);

  $move = MoveEntity::create();
  $move->setAmount($options['amount']);
  $move->setCreatedTime(time());
  $move->setDestinationAccount($destination);
  $move->setName(t('Demo move from @from to @to.', [
    '@from' => $source->getName(),
    '@to' => $destination->getName(),
  ]));
  $move->setSourceAccount($source);
  $move->save();

  $context['message'] = t('Generating moves from @from to @to.', [
    '@from' => $source->getName(),
    '@to' => $destination->getName(),
  ]);
}

/**
 * Callback when the batch processing is complete.
 *
 * @param mixed $success
 *   Success.
 * @param mixed $results
 *   Results.
 * @param mixed $operations
 *   Operations.
 */
function generate_moves_batch_completed_callback($success, $results, $operations) {
  if ($success) {
    // Do something else if needed.
    $message = t('All moves generated.');
  }
  else {
    $message = t('There were some errors.');
  }
  drupal_set_message($message);
}
