<?php

namespace Drupal\accountant;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Move entities.
 *
 * @ingroup accountant
 */
class MoveEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['created'] = $this->t('Valid from');
    $header['source'] = $this->t('Source account');
    $header['destination'] = $this->t('Destination account');
    $header['name'] = $this->t('Concept');
    $header['amount'] = $this->t('Amount');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\accountant\Entity\MoveEntity */
    /* @var $business \Drupal\accountant\Entity\AccountEntity */
    $source = $entity->getSourceAccount();
    /* @var $business \Drupal\accountant\Entity\AccountEntity */
    $destination = $entity->getDestinationAccount();

    $row['created'] = \Drupal::service('date.formatter')->format($entity->getCreatedTime(), 'medium');
    $row['source'] = Link::createFromRoute(
      $source->label(),
      'entity.account.canonical',
      ['account' => $source->id()]
    );
    $row['destination'] = Link::createFromRoute(
      $destination->label(),
      'entity.account.canonical',
      ['account' => $destination->id()]
    );
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.move.canonical',
      ['move' => $entity->id()]
    );
    $row['amount'] = $entity->getAmount();

    return $row + parent::buildRow($entity);
  }

}
