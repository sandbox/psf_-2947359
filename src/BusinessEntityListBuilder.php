<?php

namespace Drupal\accountant;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Business entities.
 *
 * @ingroup accountant
 */
class BusinessEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\accountant\Entity\BusinessEntity */
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.business.canonical',
      ['business' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
