<?php

namespace Drupal\accountant\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Business edit forms.
 *
 * @ingroup accountant
 */
class BusinessEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\accountant\Entity\BusinessEntity */
    $form = parent::buildForm($form, $form_state);

    // $entity = $this->entity;.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Business.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Business.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.business.canonical', ['business' => $entity->id()]);
  }

}
