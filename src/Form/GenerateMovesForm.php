<?php

namespace Drupal\accountant\Form;

use Drupal\accountant\Entity\BusinessEntity;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class GenerateMovesForm.
 */
class GenerateMovesForm extends FormBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'generate_moves_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Help.
    $form['head-help'] = [
      '#markup' => $this->t('This generator will create moves between accounts for the units value that you select.'),
    ];

    // Select a business.
    $query = $this->entityTypeManager->getStorage('business')->getQuery('AND');
    $business = $query->execute();
    $options = [];
    foreach ($business as $rid => $id) {
      $bus = BusinessEntity::load($rid);
      $options[$bus->id()] = $bus->getName();
    }
    $form['business'] = [
      '#type' => 'select',
      '#title' => $this->t('Select business'),
      '#options' => $options,
    ];

    // Amount.
    $form['amount'] = [
      '#type' => 'number',
      '#title' => $this->t('Amount'),
      '#default_value' => 5000,
      '#min' => 10,
    ];

    // Moves.
    $form['moves'] = [
      '#type' => 'number',
      '#title' => $this->t('Moves'),
      '#default_value' => 500,
      '#min' => 1,
    ];

    // Submit.
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $values = $form_state->getValues();
    $bus = BusinessEntity::load($values['business']);
    // At least two accounts required.
    if (count($bus->getAccounts()) <= 1) {
      $form_state->setErrorByName('business', $this->t('The business must have, at least, two accounts.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $bus = BusinessEntity::load($values['business']);
    $amount = intval($values['amount']);
    $moveAmount = $amount / intval($values['moves']);

    // Display result.
    foreach ($values as $key => $value) {
      drupal_set_message($this->t('@key: @value', ['@key' => $key, '@value' => $value]));
    }

    // Show accounts.
    drupal_set_message($this->t('Accounts:'));
    /* @var \Drupal\accountant\Entity\\AccountEntity $sourceAccount */
    $sourceAccount = NULL;
    $otherAccounts = [];
    foreach ($bus->getAccounts() as $account) {
      if ($sourceAccount == NULL) {
        $sourceAccount = $account;
      }
      else {
        $otherAccounts[] = $account;
      }
      /* @var \Drupal\accountant\Entity\AccountEntity $account */
      drupal_set_message($account->id() . ': ' . $account->getName());
    }

    // Move amount.
    drupal_set_message($this->t('Amount to move: @moveamount', ['@moveamount' => $moveAmount]));

    // Generate moves.
    $batch = [
      'init_message' => $this->t('Starting moves generation...'),
      'title' => $this->t('Generating moves'),
      'operations' => [],
      'finished' => 'generate_moves_batch_complete_callback',
      'file' => drupal_get_path('module', 'accountant') . '/src/GenerateMoves.batch.inc',
    ];
    $values['moves'] = intval($values['moves']);
    $maxAccounts = count($otherAccounts);
    $destinationIndex = 0;
    for ($i = 0; $i < $values['moves']; ++$i) {
      $batch['operations'][] = ['generate_moves_batch', [[
        'source' => $sourceAccount->id(),
        'destination' => $otherAccounts[$destinationIndex]->id(),
        'amount' => $moveAmount,
      ],
      ],
      ];
      ++$destinationIndex;
      if ($destinationIndex >= $maxAccounts) {
        $destinationIndex = 0;
      }
    }

    batch_set($batch);
  }

}
