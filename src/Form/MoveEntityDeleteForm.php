<?php

namespace Drupal\accountant\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Move entities.
 *
 * @ingroup accountant
 */
class MoveEntityDeleteForm extends ContentEntityDeleteForm {


}
