<?php

namespace Drupal\accountant\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Account entities.
 *
 * @ingroup accountant
 */
class AccountEntityDeleteForm extends ContentEntityDeleteForm {


}
