<?php

namespace Drupal\accountant\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Business entities.
 *
 * @ingroup accountant
 */
class BusinessEntityDeleteForm extends ContentEntityDeleteForm {


}
