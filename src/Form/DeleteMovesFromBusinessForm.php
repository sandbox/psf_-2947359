<?php

namespace Drupal\accountant\Form;

use Drupal\accountant\Entity\BusinessEntity;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class DeleteMovesFromBusinessForm.
 */
class DeleteMovesFromBusinessForm extends FormBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delete_moves_from_business_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Select a business.
    $query = $this->entityTypeManager->getStorage('business')->getQuery('AND');
    $business = $query->execute();
    $options = [];
    foreach ($business as $rid => $id) {
      $bus = BusinessEntity::load($rid);
      $options[$bus->id()] = $bus->getName();
    }
    $form['business'] = [
      '#type' => 'select',
      '#title' => $this->t('Select business'),
      '#options' => $options,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $bus = BusinessEntity::load($values['business']);
    $moveIds = $bus->getAllMovesIds();
    // Generate moves.
    $batch = [
      'init_message' => $this->t('Starting moves deletion...'),
      'title' => $this->t('Deleting moves'),
      'operations' => [],
      'finished' => 'delete_moves_batch_complete_callback',
      'file' => drupal_get_path('module', 'accountant') . '/src/DeleteMoves.batch.inc',
    ];
    $cnt = 0;
    $subList = [];
    $max = count($moveIds);
    for ($i = 0; $i < $max; ++$i) {
      ++$cnt;
      $subList[] = $moveIds[$i];
      if ($cnt >= 100) {
        $batch['operations'][] = ['delete_moves_batch', [$subList]];
        $subList = [];
        $cnt = 0;
      }
    }
    if (count($subList) > 0) {
      $batch['operations'][] = ['delete_moves_batch', [$subList]];
    }

    batch_set($batch);
  }

}
